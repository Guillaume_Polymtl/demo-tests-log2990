import { ComponentFixture, TestBed } from '@angular/core/testing';
import { BoardService } from 'src/app/services/board/board.service';
import { BoardComponent } from './board.component';

describe('BoardComponent', () => {
  let component: BoardComponent;
  let fixture: ComponentFixture<BoardComponent>;
  let boardServiceSpy: jasmine.SpyObj<BoardService>;

  beforeEach(async () => {
    boardServiceSpy = jasmine.createSpyObj('BoardService', ['draw']);
    await TestBed.configureTestingModule({
      declarations: [BoardComponent],
      providers: [
        { provide: BoardService, useValue: boardServiceSpy }
      ]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BoardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('ngOnInit should call draw', () => {
    const spy = spyOn(component, 'draw');
    component.ngOnInit();
    expect(spy).toHaveBeenCalledTimes(1);
  });

  it('draw should call boardService.draw', () => {
    component.draw();
    expect(boardServiceSpy.draw).toHaveBeenCalledTimes(2);
  });

});
