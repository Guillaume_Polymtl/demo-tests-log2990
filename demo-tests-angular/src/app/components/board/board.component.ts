import { Component, OnInit } from '@angular/core';
import { BoardService } from 'src/app/services/board/board.service';

@Component({
  selector: 'app-board',
  template: ''
})
export class BoardComponent implements OnInit {

  constructor(private readonly boardService: BoardService) { }

  ngOnInit(): void {
    this.draw();
  }

  draw(): void {
    this.boardService.draw();
  }
}
