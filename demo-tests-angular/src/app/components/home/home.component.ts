import { Component, OnInit } from '@angular/core';
import { ChatService } from 'src/app/services/chat/chat.service';

@Component({
  selector: 'app-home',
  template: '',
})
export class HomeComponent implements OnInit {

  messages: string[] = [];

  constructor(private readonly chatService: ChatService) { }

  ngOnInit(): void {
    this.chatService.messages.subscribe((messages: string[]) => {
      this.messages = messages;
    });
  }
}
