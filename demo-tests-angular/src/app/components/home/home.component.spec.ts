import { ComponentFixture, TestBed } from '@angular/core/testing';
import { BehaviorSubject, of } from 'rxjs';
import { ChatService } from 'src/app/services/chat/chat.service';
import { HomeComponent } from './home.component';


// tslint:disable: no-string-literal
describe('HomeComponent', () => {

  let component: HomeComponent;
  let fixture: ComponentFixture<HomeComponent>;

  const initialMessages = ['Hello', 'world'];
  const chatServiceStub: Partial<ChatService> = {
    messages: new BehaviorSubject<string[]>(initialMessages)
  };

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [HomeComponent],
      providers: [
        { provide: ChatService, useValue: chatServiceStub }
      ]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should update the messages atribute when chatService emits a new value', () => {
    const newMessages = ['newMessage'];
    chatServiceStub.messages.next(newMessages);
    expect(component.messages).toEqual(newMessages);
  });
});
