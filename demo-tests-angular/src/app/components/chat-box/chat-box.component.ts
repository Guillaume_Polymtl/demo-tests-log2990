import { Component, OnInit } from '@angular/core';
import { ChatService } from 'src/app/services/chat/chat.service';

@Component({
  selector: 'app-chat-box',
  template: '',
})
export class ChatBoxComponent {

  message = '';

  constructor(private readonly chatService: ChatService) { }

  sendMessage(message: string): void {
    const result = this.chatService.sendMessage(message);
    this.message = result + ' ' + 'world';
  }

}
