import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ChatService } from 'src/app/services/chat/chat.service';

import { ChatBoxComponent } from './chat-box.component';

describe('ChatBoxComponent', () => {
  let component: ChatBoxComponent;
  let fixture: ComponentFixture<ChatBoxComponent>;
  let chatServiceSpy: jasmine.SpyObj<ChatService>;
  const sentMessage = 'sentMessage';
  const returnValue = 'returnValue';

  beforeEach(async () => {
    chatServiceSpy = jasmine.createSpyObj('ChatService', ['sendMessage']);
    chatServiceSpy.sendMessage.and.returnValue(returnValue);
    await TestBed.configureTestingModule({
      declarations: [ChatBoxComponent],
      providers: [
        { provide: ChatService, useValue: chatServiceSpy }
      ]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ChatBoxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('sendMessage should call chatService.sendMessage', () => {
    component.sendMessage(sentMessage);
    expect(chatServiceSpy.sendMessage).toHaveBeenCalledWith(sentMessage);
  });

  it('doStuff should set the correct value for a from the return value of chatService.sendMessage', () => {
    component.sendMessage(sentMessage);
    expect(component.message).toEqual(returnValue + ' ' + 'world');
  });

});
