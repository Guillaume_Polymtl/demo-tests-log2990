import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ChatService } from 'src/app/services/chat/chat.service';
import { ChatComponent } from './chat.component';

describe('ChatComponent', () => {
  let component: ChatComponent;
  let fixture: ComponentFixture<ChatComponent>;
  const message = 'Hello world!';

  const testAServiceStub: Partial<ChatService> = {
    message,
  };

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ChatComponent],
      providers: [
        {
          provide: ChatService, useValue: testAServiceStub
        }
      ]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ChatComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('readMessage should set the correct value from ChatService', () => {
    component.readMessage();
    expect(component.message).toEqual(testAServiceStub.message);
  });

  it('addMessage should read the correct value from ChatService.message and append it to messages', () => {
    const startingLength = component.messages.length;
    component.addMessage();
    const lastMessage = component.messages[component.messages.length - 1];
    expect(startingLength).toEqual(0);
    expect(lastMessage).toEqual(message);
  });
});
