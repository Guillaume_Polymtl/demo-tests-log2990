import { Component } from '@angular/core';
import { ChatService } from 'src/app/services/chat/chat.service';

@Component({
  selector: 'app-chat',
  template: ''
})
export class ChatComponent {

  message = '';
  messages: string[] = [];

  constructor(private readonly chatService: ChatService) { }

  readMessage(): void {
    this.message = this.chatService.message;
  }

  addMessage(): void {
    this.messages.push(this.chatService.message);
  }

}
