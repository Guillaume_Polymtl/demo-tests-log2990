import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ChatService {

  readonly message: string = '';
  readonly messages: BehaviorSubject<string[]> = new BehaviorSubject<string[]>([]);

  sendMessage(message: string): string {
    // send stuff
    return message;
  }
}
