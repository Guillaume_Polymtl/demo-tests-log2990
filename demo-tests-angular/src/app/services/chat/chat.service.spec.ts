import { TestBed } from '@angular/core/testing';

import { ChatService } from './chat.service';

describe('ChatService', () => {
  let service: ChatService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ChatService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('sendMessage should return the same message it receives as parameter', () => {
    const input = 'input';
    const result = service.sendMessage(input);
    const expectation = input;
    expect(result).toEqual(expectation);
  });
});
