import { Injectable } from '@angular/core';
import { ChatService } from '../chat/chat.service';

@Injectable({
  providedIn: 'root'
})
export class TestBService {

  message = 'hello';

  constructor(private readonly chatService: ChatService) { }

  sendMessage(param: string): void {
    this.message = this.chatService.sendMessage(param);
  }

}
