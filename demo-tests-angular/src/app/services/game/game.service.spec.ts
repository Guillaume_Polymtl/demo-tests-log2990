import { TestBed } from '@angular/core/testing';
import { ChatService } from '../chat/chat.service';

import { TestBService } from './game.service';

describe('GameService', () => {
  let service: TestBService;
  // let chatServiceSpy: jasmine.SpyObj<ChatService>;

  beforeEach(() => {
    // chatServiceSpy = jasmine.createSpyObj('ChatService', ['sendMessage']);

    TestBed.configureTestingModule({
      providers: [
       //  { provide: ChatService, useValue: chatServiceSpy }
      ]
    });
    service = TestBed.inject(TestBService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('methodA should call service serviceA and set the correct value', () => {

    const chatServiceSpy = jasmine.createSpyObj('ChatService', ['sendMessage']);
    Object.assign(service['chatService'], chatServiceSpy);

    const returnValue = 'return value';
    chatServiceSpy.sendMessage.and.returnValue(returnValue);
    expect(chatServiceSpy.sendMessage).not.toHaveBeenCalled();
    const param = 'someRandomParameter';
    service.sendMessage(param);
    expect(chatServiceSpy.sendMessage).toHaveBeenCalledWith(param);
    expect(service.message).toEqual(returnValue);
  });

});
