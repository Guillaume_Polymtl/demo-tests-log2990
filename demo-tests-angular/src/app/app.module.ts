import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { TestsComponent } from './pages/tests/tests.component';
import { ChatComponent } from './components/chat/chat.component';
import { BoardComponent } from './components/board/board.component';
import { ChatBoxComponent } from './components/chat-box/chat-box.component';
import { HomeComponent } from './components/home/home.component';

@NgModule({
  declarations: [
    AppComponent,
    TestsComponent,
    ChatComponent,
    BoardComponent,
    ChatBoxComponent,
    HomeComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
