import { DictionnaryService } from '@app/services/dictionnary/dictionnary.service';
import { Request, Response, Router } from 'express';
import { Service } from 'typedi';

@Service()
export class DictionnaryController {
    router: Router;

    constructor(private readonly dictionnaryService: DictionnaryService) {
        this.configureRouter();
    }

    private configureRouter(): void {
        this.router = Router();
        this.router.get('/', (req: Request, res: Response) => {
            // Send the request to the service and send the response
            this.dictionnaryService.validateWord(req.body).then((result: boolean) => {
                res.json(result);
            });
        });
    }
}
