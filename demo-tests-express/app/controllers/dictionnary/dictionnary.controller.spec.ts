import { Application } from '@app/app';
import { DictionnaryService } from '@app/services/dictionnary/dictionnary.service';
import { expect } from 'chai';
import { StatusCodes } from 'http-status-codes';
import { createStubInstance, SinonStubbedInstance } from 'sinon';
import supertest = require('supertest');
import { Container } from 'typedi';


describe('DictionnaryController', () => {
    let dictionnaryService: SinonStubbedInstance<DictionnaryService>;
    let expressApp: Express.Application;

    const returnValue = true;

    beforeEach(async () => {
        dictionnaryService = createStubInstance(DictionnaryService);
        dictionnaryService.validateWord.resolves(returnValue);
        const app = Container.get(Application);
        Object.defineProperty(app['dictionnaryController'], 'dictionnaryService', { value: dictionnaryService, writable: true });
        expressApp = app.app;
    });

    it('should return true from ChatService on get request to root', async () => {
        return supertest(expressApp).get('/api/dictionnary')
            .expect(StatusCodes.OK)
            .then((response) => {
                expect(response.body).to.equal(returnValue);
            })
    });

});
