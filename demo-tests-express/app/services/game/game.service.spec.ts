import { expect } from 'chai';
import { createStubInstance, SinonStubbedInstance } from 'sinon';
import Sinon = require('sinon');
import { Container } from 'typedi';
import { DictionnaryService } from '../dictionnary/dictionnary.service';
import { GameService } from './game.service';

describe('GameService', () => {
    let service: GameService;
    let dictionnaryService: SinonStubbedInstance<DictionnaryService>;
    const validWord = 'valid-word';
    const invalidWord = 'invalid-word';

    beforeEach(async () => {
        dictionnaryService = createStubInstance(DictionnaryService);
        dictionnaryService.validateWord.callsFake((word: string) => {
            return Promise.resolve(word === validWord);
        });
        service = Container.get(GameService);
        Object.defineProperty(service, 'dictionnaryService', { value: dictionnaryService, writable: true });
    });

    it('should test something', () => {
        service.makeAction();
    });

    it('placeWord should call dictionnary.validateWord', async () => {
        await service.placeWord('');
        Sinon.assert.called(dictionnaryService.validateWord);
    });


    it('place word should return true for a valid word', async () => {
        const result = await service.placeWord(validWord);
        expect(result).to.equal(true);
    });

    it('place word should return false for an invalid word', async () => {
        const result = await service.placeWord(invalidWord);
        expect(result).to.equal(false);
    });


    it('placeWord should call privateMethod', () => {
        const spy = Sinon.spy(service as any, 'privateMethod');
        service.placeWord('');
        Sinon.assert.calledOnce(spy);
    });

    it('methodCallingObjectMethod should call objMethod', () => {
        const spy = Sinon.spy(service.obj, 'objMethod');
        service.methodCallingObjectMethod();
        Sinon.assert.calledOnce(spy);
    });

});
