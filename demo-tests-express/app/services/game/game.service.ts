import { Service } from 'typedi';
import { DictionnaryService } from '../dictionnary/dictionnary.service';

@Service()
export class GameService {

    obj = {
        objMethod(): string {
            return '';
        }
    }

    constructor(private readonly dictionnaryService: DictionnaryService) { }

    async makeAction(): Promise<string> {
        return 'hello world';
    }

    async placeWord(word: string): Promise<boolean> {
        this.privateMethod();
        return await this.dictionnaryService.validateWord(word);
    }

    private privateMethod(): number {
        return 0;
    }

    methodCallingObjectMethod(): void {
        this.obj.objMethod();
    }
}
