import { Service } from 'typedi';

@Service()
export class DictionnaryService {

    constructor() { }

    async validateWord(word: string): Promise<boolean> {
        if (word.length > 10 && word.includes('abc')) {
            return true;
        }
        return false;
    }
}
