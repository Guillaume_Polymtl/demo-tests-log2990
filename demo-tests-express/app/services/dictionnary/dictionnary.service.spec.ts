import { expect } from 'chai';
import { DictionnaryService } from './dictionnary.service';

describe('TestAService', () => {
    let service: DictionnaryService;

    beforeEach(async () => {
        service = new DictionnaryService();
    });

    it('validateWord should return true for a valid word', async () => {
        const result = await service.validateWord('abc-10101010101010');
        expect(result).to.equal(true);
    });

    it('validateWord should return false for an invalid word', async () => {
        const result = await service.validateWord('ab');
        expect(result).to.equal(false);
    });
});
