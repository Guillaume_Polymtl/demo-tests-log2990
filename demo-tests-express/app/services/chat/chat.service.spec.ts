import Sinon = require('sinon');
import Container from 'typedi';
import { GameService } from '../game/game.service';
import { ChatService } from './chat.service';

describe('ChatService', () => {
    let service: ChatService;

    beforeEach(async () => {
        service = Container.get(ChatService);
    });

    it('sendMessage should call gameService.makeAction if X...', async () => {
        const mock = Sinon.mock(Container.get(GameService));
        mock.expects('makeAction').once();
        await service.sendMessage();
        mock.verify();
    });
});
