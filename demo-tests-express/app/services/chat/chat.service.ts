import { Service } from 'typedi';
import { GameService } from '../game/game.service';

@Service()
export class ChatService {

    constructor(private gameService: GameService) { }

    async sendMessage(): Promise<string> {
        const action = await this.gameService.makeAction();
        return action;
    }
}
